import 'dart:async';


import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mic_task_yalla/src/utils/CustomContainer.dart';

import '../utils/keys.dart';

class ChatGroupPage extends StatefulWidget {

  ChatGroupPage();

  @override
  _ChatGroupPageState createState() => _ChatGroupPageState();
}

class _ChatGroupPageState extends State<ChatGroupPage> {
  final _users = <int>[];
  final _boolSpeakingInfo = <bool>[];
  final _infoStrings = <String>[];
  bool muted = false;
  RtcEngine _engine;
  int mikeVolume=0;
  int speakerUid;
  bool _currentUserisSpeaker = false;

  @override
  void dispose() {
    _users.clear();
    _boolSpeakingInfo.clear();
    _engine.leaveChannel();
    _engine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initialize();
  }

  Future<void> initialize() async {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in keys.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }

    await _initializeAgoraRtcEngine();
    _addAgoraEventHandlers();
    await _engine.joinChannel("006025988de152040a1bac63ef5e5068dc7IACe4Lv6ihd74HTJ1gXE5Y2q/LbV1Q2N5DDxZ2ogCrL8ADKwscgAAAAAEABXvn0FLmYBYAEAAQAvZgFg", "channeltest", null, 0);
  }

  Future<void> _initializeAgoraRtcEngine() async {
    _engine = await RtcEngine.create(APP_ID);
    await _engine.disableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Broadcaster);
    await _engine.enableAudioVolumeIndication(200, 3, true);
  }

  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, audioVolumeIndication: (audioInfo, audioVolume) {
      // AudioVolumeInfo
      setState(() {
        if(audioInfo.isNotEmpty) {
          mikeVolume = audioInfo[0].volume;
          audioInfo[0].vad == 1
              ? _currentUserisSpeaker = true
              : _currentUserisSpeaker = false;
        }
      });


      if (audioInfo.isNotEmpty) {
        print(" UID Of Speaker----"+audioInfo[0].uid.toString());
        if(_users.isNotEmpty) {
          print("OTHER USER_______________ " + _users[0].toString());

          if(_boolSpeakingInfo.isNotEmpty && audioInfo[0].uid!=0 &&mikeVolume>10) {

            int idx = _users.indexOf(audioInfo[0].uid);

            setState(() {
              _boolSpeakingInfo[idx] = true;
            });
            Timer(Duration(milliseconds: 300), () {
              _boolSpeakingInfo[idx] = false;
            });

              // _boolSpeakingInfo[idx] = false;


          }
        }
        //     "\n  VAD---"+audioInfo[0].vad.toString()+
        //     "\n Volume----"+audioInfo[0].volume.toString());
      }
      // print("Listening to Audio Changes__________ \n AudioVolume --- "+audioVolume.toString());

    }, joinChannelSuccess: (channel, uid, elapsed) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
        _boolSpeakingInfo.clear();
      });
    }, userJoined: (uid, elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
        _boolSpeakingInfo.add(false);
      });
    }, userOffline: (uid, elapsed) {
      setState(
        () {
          final info = 'userOffline: $uid';
          _infoStrings.add(info);
          _users.remove(uid);
          _boolSpeakingInfo.remove(uid);
        },
      );
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }


  Widget _toolbar() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: [

              CircleAvatar(
                backgroundColor: mikeVolume!=null?mikeVolume>10&&_currentUserisSpeaker?Colors.black:Colors.white:Colors.white,
                radius: 30,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RawMaterialButton(
                    onPressed: _onToggleMute,
                    child: Icon(
                      muted ? Icons.mic_off : Icons.mic,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    shape: CircleBorder(),
                    elevation: 2.0,
                    fillColor: Colors.blueAccent,
                    padding: const EdgeInsets.all(12.0),
                  ),
                ),
              ),
            ],
          ),
          RawMaterialButton(
            onPressed: () => _onCallEnd(context),
            child: Icon(
              Icons.call_end,
              color: Colors.white,
              size: 35.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
          ),
        ],
      ),
    );
  }


  void _onCallEnd(BuildContext context) {
    Navigator.pop(context);
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Agora Flutter QuickStart'),
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          // _panel(),
          GridView.builder(
              padding: const EdgeInsets.all(10),
              itemCount: _users.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4, crossAxisSpacing: 10, mainAxisSpacing: 10),
              itemBuilder: (ctx, i) {
                // print(products[idx].image);
                return CustomIconContainer(i,_boolSpeakingInfo[i]);
              }),
          _toolbar(),
        ],
      ),
    );
  }
}

// Widget _panel() {
//   return Container(
//     padding: const EdgeInsets.symmetric(vertical: 48),
//     alignment: Alignment.bottomCenter,
//     child: FractionallySizedBox(
//       heightFactor: 0.5,
//       child: Container(
//         padding: const EdgeInsets.symmetric(vertical: 48),
//         child: ListView.builder(
//           reverse: true,
//           itemCount: _infoStrings.length,
//           itemBuilder: (BuildContext context, int index) {
//             if (_infoStrings.isEmpty) {
//               return null;
//             }
//             return Padding(
//               padding: const EdgeInsets.symmetric(
//                 vertical: 3,
//                 horizontal: 10,
//               ),
//               child: Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   Flexible(
//                     child: Container(
//                       padding: const EdgeInsets.symmetric(
//                         vertical: 2,
//                         horizontal: 5,
//                       ),
//                       decoration: BoxDecoration(
//                         color: Colors.yellowAccent,
//                         borderRadius: BorderRadius.circular(5),
//                       ),
//                       child: Text(
//                         _infoStrings[index],
//                         style: TextStyle(color: Colors.blueGrey),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             );
//           },
//         ),
//       ),
//     ),
//   );
// }


