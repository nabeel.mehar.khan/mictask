import 'dart:async';

import 'package:flutter/material.dart';

class CustomIconContainer extends StatefulWidget {



  @override
  _CustomIconContainerState createState() => _CustomIconContainerState();

  int idx;
  bool boolSpeakingInfo;

  CustomIconContainer(this.idx,this.boolSpeakingInfo);
}

class _CustomIconContainerState extends State<CustomIconContainer> {
  @override
  Widget build(BuildContext context) {



    return Column(
      children: [
        CircleAvatar(
          radius: 30,
          backgroundColor:widget.boolSpeakingInfo?Colors.black:Colors.white,
          child: RawMaterialButton(
            onPressed : (){},
            child: Icon(Icons.mic,
              color:  Colors.white,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor:Colors.blueAccent,
            padding: const EdgeInsets.all(12.0),
          ),
        ),
        Text("User "+(widget.idx+1).toString())
      ],
    );
  }
}
